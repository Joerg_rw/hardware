# Hardware

KiCad version 5.0 or newer is required, 5.1 is recommended.

If you are reading this, you are likely also wondering how to get access to push to this repo.
Currently the repo is read-only, please get in touch directly
via Matrix ( #card10badge:asra.gr ) or IRC ( freenode.com#card10badge, mirror)
if you want to contribute to the hardware.


The project contains two PCBs:

The Fundamental Board:
 - KiCad project file: `boards/Fundamental-Board/Fundamental-Board.pro`


The Harmonic Board:
 - KiCad project file: `boards/Harmonic-Board/Harmonic-Board.pro`
