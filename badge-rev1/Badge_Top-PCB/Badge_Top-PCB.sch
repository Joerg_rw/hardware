EESchema Schematic File Version 4
LIBS:Badge_Top-PCB-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1300 1600 1750 2400
U 5CB104CE
F0 "EKG" 50
F1 "EKG.sch" 50
$EndSheet
$Sheet
S 3650 1600 1800 2400
U 5CB104E6
F0 "Connections" 50
F1 "Connections.sch" 50
$EndSheet
$Sheet
S 5950 1600 1550 2400
U 5D345ECD
F0 "Display Section" 50
F1 "Display.sch" 50
$EndSheet
$Sheet
S 7900 1600 2250 2400
U 5D3957BE
F0 "RGB-LEDs" 50
F1 "LED.sch" 50
$EndSheet
$EndSCHEMATC
