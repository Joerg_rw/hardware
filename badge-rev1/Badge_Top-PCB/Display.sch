EESchema Schematic File Version 4
LIBS:Badge_Top-PCB-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 6900 3700 2    50   Input ~ 0
SPI_CS
Text GLabel 6900 3300 2    50   Input ~ 0
SPI_MOSI
Text GLabel 8200 4450 2    50   Input ~ 0
GPIO4
Text Notes 7500 3750 0    50   ~ 0
Display CS
Text Notes 7500 3350 0    50   ~ 0
Display MOSI / DC
Text Notes 8600 4450 0    50   ~ 0
Display Backlight
Text GLabel 6900 3400 2    50   Input ~ 0
SPI_SCK
Text Notes 7500 3450 0    50   ~ 0
Display Clock
$Comp
L power:+3V3 #PWR?
U 1 1 5D3D02E0
P 6650 2750
AR Path="/5CB104E6/5D3D02E0" Ref="#PWR?"  Part="1" 
AR Path="/5D345ECD/5D3D02E0" Ref="#PWR0123"  Part="1" 
F 0 "#PWR0123" H 6650 2600 50  0001 C CNN
F 1 "+3V3" H 6665 2923 50  0000 C CNN
F 2 "" H 6650 2750 50  0001 C CNN
F 3 "" H 6650 2750 50  0001 C CNN
	1    6650 2750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D3D02EF
P 7950 4950
AR Path="/5CB104E6/5D3D02EF" Ref="#PWR?"  Part="1" 
AR Path="/5D345ECD/5D3D02EF" Ref="#PWR0124"  Part="1" 
F 0 "#PWR0124" H 7950 4700 50  0001 C CNN
F 1 "GND" V 7955 4822 50  0000 R CNN
F 2 "" H 7950 4950 50  0001 C CNN
F 3 "" H 7950 4950 50  0001 C CNN
	1    7950 4950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5D3D1826
P 4800 3550
F 0 "C9" H 4825 3650 50  0000 L CNN
F 1 "C" H 4825 3450 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4838 3400 50  0001 C CNN
F 3 "~" H 4800 3550 50  0001 C CNN
	1    4800 3550
	1    0    0    -1  
$EndComp
$Comp
L 0.96_IPS_display_china:ST7735_IPS_Display Disp1
U 1 1 5CDBC3A9
P 6150 3650
F 0 "Disp1" H 6231 4465 50  0000 C CNN
F 1 "ST7735_IPS_Display" H 6231 4374 50  0000 C CNN
F 2 "china:KD0096FM-1" H 6150 3650 50  0001 C CNN
F 3 "" H 6150 3650 50  0001 C CNN
	1    6150 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4300 6450 4300
Wire Wire Line
	6750 4300 6750 3800
Wire Wire Line
	6750 3800 6450 3800
Connection ~ 6750 4300
Wire Wire Line
	6450 3700 6900 3700
Wire Wire Line
	6900 3300 6450 3300
Wire Wire Line
	6450 3400 6900 3400
Wire Wire Line
	6450 4000 6650 4000
Text GLabel 6900 3600 2    50   Input ~ 0
PMIC_GPIO
$Comp
L Device:R R5
U 1 1 5CDBC939
P 6800 3000
F 0 "R5" V 6880 3000 50  0000 C CNN
F 1 "47k" V 6800 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_01005_0402Metric" V 6730 3000 50  0001 C CNN
F 3 "~" H 6800 3000 50  0001 C CNN
	1    6800 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6800 2850 6800 2800
Wire Wire Line
	6800 2800 6650 2800
Connection ~ 6650 2800
Wire Wire Line
	6650 2800 6650 2750
Wire Wire Line
	6450 3600 6800 3600
Wire Wire Line
	6800 3150 6800 3600
Connection ~ 6800 3600
Wire Wire Line
	6800 3600 6900 3600
Text Notes 7500 3650 0    50   ~ 0
Display Reset
$Comp
L power:GND #PWR?
U 1 1 5CDBD56A
P 4800 3900
AR Path="/5CB104E6/5CDBD56A" Ref="#PWR?"  Part="1" 
AR Path="/5D345ECD/5CDBD56A" Ref="#PWR0134"  Part="1" 
F 0 "#PWR0134" H 4800 3650 50  0001 C CNN
F 1 "GND" V 4805 3772 50  0000 R CNN
F 2 "" H 4800 3900 50  0001 C CNN
F 3 "" H 4800 3900 50  0001 C CNN
	1    4800 3900
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR?
U 1 1 5CDBD585
P 4800 3200
AR Path="/5CB104E6/5CDBD585" Ref="#PWR?"  Part="1" 
AR Path="/5D345ECD/5CDBD585" Ref="#PWR0135"  Part="1" 
F 0 "#PWR0135" H 4800 3050 50  0001 C CNN
F 1 "+3V3" H 4815 3373 50  0000 C CNN
F 2 "" H 4800 3200 50  0001 C CNN
F 3 "" H 4800 3200 50  0001 C CNN
	1    4800 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4800 3400 4800 3200
Wire Wire Line
	4800 3900 4800 3700
Text Notes 4600 3950 1    50   ~ 0
Place next to Display
Wire Wire Line
	6450 4200 6650 4200
Wire Wire Line
	6650 2800 6650 4000
Connection ~ 6650 4000
Wire Wire Line
	6650 4000 6650 4200
Text Notes 3950 5750 0    50   ~ 0
display: KD0096FM-1\nSource: http://www.startek-lcd.com/res/starteklcd/pdres/201902/20190223094535700.pdf
$Comp
L Transistor_FET:2N7002 Q1
U 1 1 5CEA1C27
P 7600 4450
F 0 "Q1" H 7806 4496 50  0000 L CNN
F 1 "2N7002" H 7806 4405 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7800 4375 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/2N/2N7002.pdf" H 7600 4450 50  0001 L CNN
	1    7600 4450
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R9
U 1 1 5CEA1C59
P 7950 4700
F 0 "R9" V 8030 4700 50  0000 C CNN
F 1 "47k" V 7950 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_01005_0402Metric" V 7880 4700 50  0001 C CNN
F 3 "~" H 7950 4700 50  0001 C CNN
	1    7950 4700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7800 4450 7950 4450
Wire Wire Line
	7950 4450 7950 4550
Wire Wire Line
	7950 4850 7950 4900
Wire Wire Line
	7950 4450 8200 4450
Connection ~ 7950 4450
Wire Wire Line
	7950 4900 7500 4900
Wire Wire Line
	7500 4900 7500 4650
Connection ~ 7950 4900
Wire Wire Line
	7950 4900 7950 4950
Wire Wire Line
	7500 4250 7500 4100
Wire Wire Line
	6750 4900 7500 4900
Wire Wire Line
	6750 4300 6750 4900
Connection ~ 7500 4900
$Comp
L Device:R R10
U 1 1 5CEAE15B
P 7200 4100
F 0 "R10" V 7280 4100 50  0000 C CNN
F 1 "10" V 7200 4100 50  0000 C CNN
F 2 "Resistor_SMD:R_01005_0402Metric" V 7130 4100 50  0001 C CNN
F 3 "~" H 7200 4100 50  0001 C CNN
	1    7200 4100
	0    1    -1   0   
$EndComp
Wire Wire Line
	7500 4100 7350 4100
Wire Wire Line
	7050 4100 6450 4100
Text GLabel 6900 3500 2    50   Input ~ 0
GPIO1
Wire Wire Line
	6450 3500 6900 3500
$EndSCHEMATC
