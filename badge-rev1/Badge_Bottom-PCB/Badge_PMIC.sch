EESchema Schematic File Version 4
LIBS:Badge_Bottom-PCB-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 5
Title "Badge_PMIC"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR?
U 1 1 5C7D819C
P 5650 5100
AR Path="/5C7D819C" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5C7D819C" Ref="#PWR0206"  Part="1" 
F 0 "#PWR0206" H 5650 4850 50  0001 C CNN
F 1 "GND" H 5650 4950 50  0000 C CNN
F 2 "" H 5650 5100 50  0001 C CNN
F 3 "" H 5650 5100 50  0001 C CNN
	1    5650 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C7D81A2
P 5750 5100
AR Path="/5C7D81A2" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5C7D81A2" Ref="#PWR0207"  Part="1" 
F 0 "#PWR0207" H 5750 4850 50  0001 C CNN
F 1 "GND" H 5750 4950 50  0000 C CNN
F 2 "" H 5750 5100 50  0001 C CNN
F 3 "" H 5750 5100 50  0001 C CNN
	1    5750 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C7D81A8
P 5850 5100
AR Path="/5C7D81A8" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5C7D81A8" Ref="#PWR0208"  Part="1" 
F 0 "#PWR0208" H 5850 4850 50  0001 C CNN
F 1 "GND" H 5850 4950 50  0000 C CNN
F 2 "" H 5850 5100 50  0001 C CNN
F 3 "" H 5850 5100 50  0001 C CNN
	1    5850 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 5000 5650 5100
Wire Wire Line
	5750 5000 5750 5100
Wire Wire Line
	5850 5100 5850 5000
Wire Wire Line
	6550 4200 6900 4200
Wire Wire Line
	6550 4400 6600 4400
$Comp
L power:GND #PWR?
U 1 1 5C7D81D6
P 6600 5100
AR Path="/5C7D81D6" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5C7D81D6" Ref="#PWR0209"  Part="1" 
F 0 "#PWR0209" H 6600 4850 50  0001 C CNN
F 1 "GND" H 6600 4950 50  0000 C CNN
F 2 "" H 6600 5100 50  0001 C CNN
F 3 "" H 6600 5100 50  0001 C CNN
	1    6600 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C7D81DC
P 6750 5100
AR Path="/5C7D81DC" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5C7D81DC" Ref="#PWR0210"  Part="1" 
F 0 "#PWR0210" H 6750 4850 50  0001 C CNN
F 1 "GND" H 6750 4950 50  0000 C CNN
F 2 "" H 6750 5100 50  0001 C CNN
F 3 "" H 6750 5100 50  0001 C CNN
	1    6750 5100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C7D81E2
P 6900 5100
AR Path="/5C7D81E2" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5C7D81E2" Ref="#PWR0211"  Part="1" 
F 0 "#PWR0211" H 6900 4850 50  0001 C CNN
F 1 "GND" H 6900 4950 50  0000 C CNN
F 2 "" H 6900 5100 50  0001 C CNN
F 3 "" H 6900 5100 50  0001 C CNN
	1    6900 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 5100 6600 4950
Wire Wire Line
	6750 4950 6750 5100
Wire Wire Line
	6900 5100 6900 4950
Wire Wire Line
	6600 4750 6600 4400
Wire Wire Line
	6750 4750 6750 4300
Wire Wire Line
	6750 4300 6550 4300
Wire Wire Line
	6900 4750 6900 4200
Connection ~ 6900 4200
Wire Wire Line
	7950 4100 6550 4100
$Comp
L Device:C_Small C?
U 1 1 5C7D820B
P 4700 3100
AR Path="/5C7D820B" Ref="C?"  Part="1" 
AR Path="/5C7CF52B/5C7D820B" Ref="C204"  Part="1" 
F 0 "C204" H 4710 3170 50  0000 L CNN
F 1 "1uF" V 4750 2850 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 4700 3100 50  0001 C CNN
F 3 "~" H 4700 3100 50  0001 C CNN
	1    4700 3100
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C7D8212
P 4350 3200
AR Path="/5C7D8212" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5C7D8212" Ref="#PWR0203"  Part="1" 
F 0 "#PWR0203" H 4350 2950 50  0001 C CNN
F 1 "GND" H 4350 3050 50  0000 C CNN
F 2 "" H 4350 3200 50  0001 C CNN
F 3 "" H 4350 3200 50  0001 C CNN
	1    4350 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 3100 4800 3100
Wire Wire Line
	4600 3100 4350 3100
Wire Wire Line
	6600 4400 7250 4400
Connection ~ 6600 4400
Wire Wire Line
	6750 4300 7250 4300
Connection ~ 6750 4300
Wire Wire Line
	6900 4200 7250 4200
Wire Wire Line
	6550 3400 7250 3400
Wire Wire Line
	6550 3100 6850 3100
Wire Wire Line
	6550 3200 6700 3200
Wire Wire Line
	6550 3300 7250 3300
$Comp
L Device:R R?
U 1 1 5C7D8238
P 4650 4200
AR Path="/5C7D8238" Ref="R?"  Part="1" 
AR Path="/5C7CF52B/5C7D8238" Ref="R202"  Part="1" 
F 0 "R202" V 4700 4050 50  0000 C CNN
F 1 "10k" V 4650 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4580 4200 50  0001 C CNN
F 3 "~" H 4650 4200 50  0001 C CNN
	1    4650 4200
	0    1    1    0   
$EndComp
$Comp
L Device:Thermistor_NTC R?
U 1 1 5C7D823F
P 4400 4600
AR Path="/5C7D823F" Ref="R?"  Part="1" 
AR Path="/5C7CF52B/5C7D823F" Ref="R201"  Part="1" 
F 0 "R201" H 4550 4450 50  0000 C CNN
F 1 "10k NTC" H 4600 4700 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 4330 4600 50  0001 C CNN
F 3 "" H 4400 4600 50  0001 C CNN
F 4 "ERT-JZEG103FA" V 4400 4600 50  0001 C CNN "Part Number"
	1    4400 4600
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C7D8246
P 4700 2950
AR Path="/5C7D8246" Ref="C?"  Part="1" 
AR Path="/5C7CF52B/5C7D8246" Ref="C203"  Part="1" 
F 0 "C203" H 4710 3020 50  0000 L CNN
F 1 "4,7uF" V 4750 2700 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4700 2950 50  0001 C CNN
F 3 "~" H 4700 2950 50  0001 C CNN
	1    4700 2950
	0    1    1    0   
$EndComp
Wire Wire Line
	4800 2950 4950 2950
Wire Wire Line
	4950 2950 4950 3000
Wire Wire Line
	4600 2950 4350 2950
Wire Wire Line
	4350 2950 4350 3100
Connection ~ 4350 3100
Wire Wire Line
	4950 4200 4800 4200
Wire Wire Line
	4500 4200 4400 4200
$Comp
L power:GND #PWR?
U 1 1 5C7D8264
P 4400 5050
AR Path="/5C7D8264" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5C7D8264" Ref="#PWR0204"  Part="1" 
F 0 "#PWR0204" H 4400 4800 50  0001 C CNN
F 1 "GND" H 4400 4900 50  0000 C CNN
F 2 "" H 4400 5050 50  0001 C CNN
F 3 "" H 4400 5050 50  0001 C CNN
	1    4400 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4950 4300 4400 4300
Wire Wire Line
	4400 4300 4400 4200
$Comp
L Device:C_Small C?
U 1 1 5C7D8276
P 3950 4600
AR Path="/5C7D8276" Ref="C?"  Part="1" 
AR Path="/5C7CF52B/5C7D8276" Ref="C202"  Part="1" 
F 0 "C202" H 3960 4670 50  0000 L CNN
F 1 "22uF" H 3960 4520 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3950 4600 50  0001 C CNN
F 3 "~" H 3950 4600 50  0001 C CNN
	1    3950 4600
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5C7D827D
P 3950 5050
AR Path="/5C7D827D" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5C7D827D" Ref="#PWR0202"  Part="1" 
F 0 "#PWR0202" H 3950 4800 50  0001 C CNN
F 1 "GND" H 3950 4900 50  0000 C CNN
F 2 "" H 3950 5050 50  0001 C CNN
F 3 "" H 3950 5050 50  0001 C CNN
	1    3950 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 5050 3950 4700
Wire Wire Line
	3950 4500 3950 4000
Connection ~ 3950 4000
Wire Wire Line
	3950 4000 4950 4000
Wire Wire Line
	3950 1800 6550 1800
$Comp
L Device:C_Small C?
U 1 1 5C7D8290
P 3500 4600
AR Path="/5C7D8290" Ref="C?"  Part="1" 
AR Path="/5C7CF52B/5C7D8290" Ref="C201"  Part="1" 
F 0 "C201" H 3510 4670 50  0000 L CNN
F 1 "4,7uF" H 3510 4520 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 3500 4600 50  0001 C CNN
F 3 "~" H 3500 4600 50  0001 C CNN
	1    3500 4600
	-1   0    0    1   
$EndComp
Wire Wire Line
	3350 4100 3500 4100
Wire Wire Line
	3500 4500 3500 4100
Connection ~ 3500 4100
Wire Wire Line
	3500 4100 4950 4100
$Comp
L power:GND #PWR?
U 1 1 5C7D829B
P 3500 5050
AR Path="/5C7D829B" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5C7D829B" Ref="#PWR0201"  Part="1" 
F 0 "#PWR0201" H 3500 4800 50  0001 C CNN
F 1 "GND" H 3500 4900 50  0000 C CNN
F 2 "" H 3500 5050 50  0001 C CNN
F 3 "" H 3500 5050 50  0001 C CNN
	1    3500 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 5050 3500 4700
Wire Wire Line
	4400 5050 4400 4750
Wire Wire Line
	4400 4450 4400 4300
Connection ~ 4400 4300
$Comp
L Device:C_Small C?
U 1 1 5C7D82A6
P 5750 2200
AR Path="/5C7D82A6" Ref="C?"  Part="1" 
AR Path="/5C7CF52B/5C7D82A6" Ref="C206"  Part="1" 
F 0 "C206" H 5760 2270 50  0000 L CNN
F 1 "3,3nF" H 5760 2120 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 5750 2200 50  0001 C CNN
F 3 "~" H 5750 2200 50  0001 C CNN
	1    5750 2200
	-1   0    0    1   
$EndComp
$Comp
L Device:L L?
U 1 1 5C7D82AD
P 5950 2150
AR Path="/5C7D82AD" Ref="L?"  Part="1" 
AR Path="/5C7CF52B/5C7D82AD" Ref="L201"  Part="1" 
F 0 "L201" V 5900 2150 50  0000 C CNN
F 1 "1,5uH" V 6025 2150 50  0000 C CNN
F 2 "coilcraft:L_Coilcraft_XPL2010" H 5950 2150 50  0001 C CNN
F 3 "~" H 5950 2150 50  0001 C CNN
F 4 "XPL2010-152MLC" V 5950 2150 50  0001 C CNN "Part Number"
	1    5950 2150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 1800 7950 4100
Wire Wire Line
	5750 2400 5750 2300
Wire Wire Line
	5950 1950 5950 2000
Wire Wire Line
	5850 2400 5850 1950
Wire Wire Line
	5850 1950 5950 1950
Wire Wire Line
	5950 2400 5950 2350
Wire Wire Line
	4850 2300 5150 2300
Wire Wire Line
	5450 2300 5450 2400
Wire Wire Line
	4850 2550 4950 2550
Wire Wire Line
	4950 2550 4950 2950
Connection ~ 4950 2950
Wire Wire Line
	6550 3600 7250 3600
Wire Wire Line
	6550 3500 7250 3500
Text Notes 7100 2700 0    50   ~ 10
I²C-Slave-Address:\nADDR=0: 0x48\nADDR=1: 0x40
$Comp
L Device:R R?
U 1 1 5C7EB4DB
P 6700 2650
AR Path="/5C7EB4DB" Ref="R?"  Part="1" 
AR Path="/5C7CF52B/5C7EB4DB" Ref="R204"  Part="1" 
F 0 "R204" V 6750 2500 50  0000 C CNN
F 1 "100k" V 6700 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 6630 2650 50  0001 C CNN
F 3 "~" H 6700 2650 50  0001 C CNN
	1    6700 2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	7250 2250 6850 2250
Wire Wire Line
	6700 2250 6700 2500
Wire Wire Line
	6700 2800 6700 3200
Connection ~ 6700 3200
Wire Wire Line
	6700 3200 7250 3200
$Comp
L Device:R R?
U 1 1 5C7F1080
P 6550 2650
AR Path="/5C7F1080" Ref="R?"  Part="1" 
AR Path="/5C7CF52B/5C7F1080" Ref="R203"  Part="1" 
F 0 "R203" V 6600 2500 50  0000 C CNN
F 1 "10k" V 6550 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 6480 2650 50  0001 C CNN
F 3 "~" H 6550 2650 50  0001 C CNN
	1    6550 2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	3950 4000 3350 4000
Wire Wire Line
	6550 2500 6550 1800
Connection ~ 6550 1800
Wire Wire Line
	6550 1800 7950 1800
Wire Wire Line
	6550 2800 6550 3000
Wire Wire Line
	5750 2100 5750 1900
Wire Wire Line
	5750 1900 6100 1900
Wire Wire Line
	6100 1900 6100 2350
Wire Wire Line
	6100 2350 5950 2350
Connection ~ 5950 2350
Wire Wire Line
	5950 2350 5950 2300
Wire Wire Line
	6550 3900 6750 3900
Wire Wire Line
	3950 1800 3950 4000
Text HLabel 7250 2250 2    50   Input ~ 0
1V8
Text HLabel 7250 4200 2    50   Input ~ 0
1V8
Text HLabel 7250 4300 2    50   Input ~ 0
3V3
Text HLabel 7250 4400 2    50   Input ~ 0
5V0
Text HLabel 7250 3000 2    50   Input ~ 0
PMIC_nEN
Text HLabel 7250 3100 2    50   Input ~ 0
PMIC_nIRQ
Text HLabel 7250 3200 2    50   Input ~ 0
PMIC_nRST
Text HLabel 7250 3300 2    50   Input ~ 0
PMIC_PWR_HLD
Text HLabel 7250 3400 2    50   Input ~ 0
PMIC_GPIO
Text HLabel 7250 3600 2    50   Input ~ 0
SDA
Text HLabel 7250 3500 2    50   Input ~ 0
SCL
Text HLabel 4850 2300 0    50   Input ~ 0
1V8
Text HLabel 3350 4000 0    50   Input ~ 0
VSYS
Text HLabel 3350 4400 0    50   Input ~ 0
PMIC_AMUX
$Comp
L Device:R R?
U 1 1 5CABA3CE
P 6850 2650
AR Path="/5CABA3CE" Ref="R?"  Part="1" 
AR Path="/5C7CF52B/5CABA3CE" Ref="R205"  Part="1" 
F 0 "R205" V 6900 2500 50  0000 C CNN
F 1 "100k" V 6850 2650 50  0000 C CNN
F 2 "Resistor_SMD:R_0201_0603Metric" V 6780 2650 50  0001 C CNN
F 3 "~" H 6850 2650 50  0001 C CNN
	1    6850 2650
	-1   0    0    1   
$EndComp
Wire Wire Line
	6850 2500 6850 2250
Connection ~ 6850 2250
Wire Wire Line
	6850 2250 6700 2250
Wire Wire Line
	6850 2800 6850 3100
Connection ~ 6850 3100
Wire Wire Line
	6850 3100 7250 3100
Wire Wire Line
	4350 3100 4350 3200
Text Notes 2850 3650 1    50   ~ 0
UV-LEDs
Wire Wire Line
	3350 3400 4950 3400
Wire Wire Line
	4950 3500 3350 3500
Wire Wire Line
	3350 3600 4950 3600
Text HLabel 3350 3400 0    50   Input ~ 0
UV_LED0
Text HLabel 3350 3500 0    50   Input ~ 0
UV_LED1
Text HLabel 3350 3600 0    50   Input ~ 0
UV_LED2
Text HLabel 4850 2550 0    50   Input ~ 0
CHG_IN
Text HLabel 3350 4100 0    50   Input ~ 0
BAT+
$Comp
L Device:C_Small C?
U 1 1 5E02ADCA
P 7950 4350
AR Path="/5E02ADCA" Ref="C?"  Part="1" 
AR Path="/5C7CF52B/5E02ADCA" Ref="C211"  Part="1" 
F 0 "C211" H 7960 4420 50  0000 L CNN
F 1 "10uF" V 8000 4100 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7950 4350 50  0001 C CNN
F 3 "~" H 7950 4350 50  0001 C CNN
	1    7950 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E02AE24
P 7950 4650
AR Path="/5E02AE24" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5E02AE24" Ref="#PWR0213"  Part="1" 
F 0 "#PWR0213" H 7950 4400 50  0001 C CNN
F 1 "GND" H 7950 4500 50  0000 C CNN
F 2 "" H 7950 4650 50  0001 C CNN
F 3 "" H 7950 4650 50  0001 C CNN
	1    7950 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	7950 4100 7950 4250
Connection ~ 7950 4100
Wire Wire Line
	7950 4450 7950 4650
$Comp
L Device:C_Small C?
U 1 1 5E0302F7
P 5150 2450
AR Path="/5E0302F7" Ref="C?"  Part="1" 
AR Path="/5C7CF52B/5E0302F7" Ref="C205"  Part="1" 
F 0 "C205" H 5160 2520 50  0000 L CNN
F 1 "100nF" H 5160 2370 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201_0603Metric" H 5150 2450 50  0001 C CNN
F 3 "~" H 5150 2450 50  0001 C CNN
	1    5150 2450
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5E030355
P 5150 2650
AR Path="/5E030355" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5E030355" Ref="#PWR0205"  Part="1" 
F 0 "#PWR0205" H 5150 2400 50  0001 C CNN
F 1 "GND" H 5150 2500 50  0000 C CNN
F 2 "" H 5150 2650 50  0001 C CNN
F 3 "" H 5150 2650 50  0001 C CNN
	1    5150 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 2300 5150 2350
Connection ~ 5150 2300
Wire Wire Line
	5150 2300 5450 2300
Wire Wire Line
	5150 2550 5150 2650
$Comp
L MAX77650:MAX77650 U?
U 1 1 5C7D8288
P 5750 4700
AR Path="/5C7D8288" Ref="U?"  Part="1" 
AR Path="/5C7CF52B/5C7D8288" Ref="U201"  Part="1" 
F 0 "U201" H 4765 4600 50  0000 L CNN
F 1 "MAX77651BEWVA+T" H 4765 4521 50  0000 L CNN
F 2 "maxim:MAXIM_WLP-30_2.75x2.15mm" H 5750 4700 50  0001 C CNN
F 3 "DOCUMENTATION" H 5750 4700 50  0001 C CNN
	1    5750 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3350 4400 4950 4400
Wire Wire Line
	6550 3000 7250 3000
Connection ~ 6550 3000
Text Notes 3300 3850 0    50   ~ 0
Value of R2 equals to NTC-value @ room temp
Text Notes 8150 3100 0    50   ~ 0
PMIC_PWR_HLD has \nto be HIGH after boot \nto keep power rails on.
Text Notes 8150 3650 0    50   ~ 0
PMIC_GPIO\nis a push-pull\nstage - no need \nof pull-up-R.
$Comp
L power:GND #PWR?
U 1 1 5D160868
P 6750 3900
AR Path="/5D160868" Ref="#PWR?"  Part="1" 
AR Path="/5C7CF52B/5D160868" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 6750 3650 50  0001 C CNN
F 1 "GND" H 6750 3750 50  0000 C CNN
F 2 "" H 6750 3900 50  0001 C CNN
F 3 "" H 6750 3900 50  0001 C CNN
	1    6750 3900
	0    -1   -1   0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C7D81CF
P 6900 4850
AR Path="/5C7D81CF" Ref="C?"  Part="1" 
AR Path="/5C7CF52B/5C7D81CF" Ref="C209"  Part="1" 
F 0 "C209" V 6950 4900 50  0000 L CNN
F 1 "22uF" V 6950 4600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6900 4850 50  0001 C CNN
F 3 "~" H 6900 4850 50  0001 C CNN
	1    6900 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C7D81C8
P 6600 4850
AR Path="/5C7D81C8" Ref="C?"  Part="1" 
AR Path="/5C7CF52B/5C7D81C8" Ref="C207"  Part="1" 
F 0 "C207" V 6650 4900 50  0000 L CNN
F 1 "22uF" V 6650 4600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6600 4850 50  0001 C CNN
F 3 "~" H 6600 4850 50  0001 C CNN
	1    6600 4850
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 5C7D81C1
P 6750 4850
AR Path="/5C7D81C1" Ref="C?"  Part="1" 
AR Path="/5C7CF52B/5C7D81C1" Ref="C208"  Part="1" 
F 0 "C208" V 6800 4900 50  0000 L CNN
F 1 "22uF" V 6800 4600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6750 4850 50  0001 C CNN
F 3 "~" H 6750 4850 50  0001 C CNN
	1    6750 4850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
