EESchema Schematic File Version 4
LIBS:Badge_Bottom-PCB-cache
EELAYER 29 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 5
Title "Badge Overview"
Date "2019-03-02"
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 9000 1000 3000 1950
U 5C7CF52B
F0 "Badge_PMIC" 50
F1 "Badge_PMIC.sch" 50
F2 "PMIC_AMUX" I L 9000 2200 50 
F3 "SDA" I L 9000 2400 50 
F4 "SCL" I L 9000 2500 50 
F5 "PMIC_LDO" I R 12000 1150 50 
F6 "PMIC_nRST" I L 9000 2600 50 
F7 "PMIC_nEN" I L 9000 2750 50 
F8 "PMIC_nIRQ" I L 9000 2300 50 
F9 "PMIC_PWR_HLD" I L 9000 2100 50 
F10 "PMIC_GPIO" I L 9000 1950 50 
F11 "1V8" I L 9000 1450 50 
F12 "VSYS" I R 12000 1250 50 
F13 "3V3" I L 9000 1550 50 
F14 "5V0" I L 9000 1650 50 
F15 "UV_LED0" I L 9000 1100 50 
F16 "UV_LED1" I L 9000 1200 50 
F17 "UV_LED2" I L 9000 1300 50 
F18 "CHG_IN" I L 9000 1850 50 
F19 "BAT+" I L 9000 1750 50 
$EndSheet
$Sheet
S 9000 6450 3000 2000
U 5C7E132D
F0 "Badge_Sensors_ECG" 50
F1 "Badge_Sensors_ECG.sch" 50
F2 "ECG_CSB" I L 9000 7500 50 
F3 "ECG_SDI" I L 9000 7600 50 
F4 "ECG_SCLK" I L 9000 7800 50 
F5 "ECG_SDO" I L 9000 7700 50 
F6 "ECG_INT" I L 9000 7900 50 
F7 "ECG_INT2" I L 9000 8000 50 
F8 "ECG_FCLK" I L 9000 8200 50 
F9 "ECG_AOUT" I L 9000 8300 50 
F10 "1V8" I L 9000 7000 50 
F11 "ECG_P" I L 9000 6550 50 
F12 "ECP_N" I L 9000 6650 50 
F13 "ECG_VCM" I L 9000 6750 50 
$EndSheet
Text Notes 2600 3550 0    197  ~ 0
uC Section
Wire Wire Line
	9000 2100 6250 2100
Wire Wire Line
	6250 2200 9000 2200
Wire Wire Line
	9000 2300 6250 2300
Wire Wire Line
	9000 1450 7800 1450
Wire Wire Line
	7800 1450 6250 1450
Text Notes 9700 2350 0    197  ~ 0
PMIC Section
Text Notes 9750 4650 0    197  ~ 0
Motion Sensor\nSection
$Sheet
S 9000 3450 3000 1400
U 5C7D379B
F0 "Badge_Sensors_Environment" 50
F1 "Badge_Sensors_Environment.sch" 50
F2 "1V8" I L 9000 3650 50 
F3 "SDA" I L 9000 4400 50 
F4 "SCL" I L 9000 4500 50 
F5 "BHI160_INT" I L 9000 4100 50 
F6 "BMA400_INT1" I L 9000 4200 50 
F7 "BMA400_INT2" I L 9000 4300 50 
$EndSheet
Wire Wire Line
	7800 3650 9000 3650
Wire Wire Line
	9000 4100 7500 4100
Wire Wire Line
	7500 4100 7500 3400
Wire Wire Line
	7500 3400 6250 3400
Wire Wire Line
	6250 3500 7400 3500
Wire Wire Line
	7400 3500 7400 4200
Wire Wire Line
	7400 4200 9000 4200
Wire Wire Line
	9000 4400 8200 4400
Wire Wire Line
	7200 4400 7200 3700
Wire Wire Line
	7200 3700 6250 3700
Wire Wire Line
	6250 3800 7100 3800
Wire Wire Line
	7100 3800 7100 4500
Wire Wire Line
	7100 4500 8100 4500
Text Notes 9750 6950 0    197  ~ 0
ECG Section
Wire Wire Line
	7800 7000 9000 7000
Wire Wire Line
	9000 7500 7550 7500
Wire Wire Line
	7550 4650 6250 4650
Wire Wire Line
	9000 7600 7450 7600
Wire Wire Line
	7450 4750 6250 4750
Wire Wire Line
	9000 7700 7350 7700
Wire Wire Line
	7350 4850 6250 4850
Wire Wire Line
	9000 7800 7250 7800
Wire Wire Line
	7250 4950 6250 4950
Wire Wire Line
	9000 7900 7150 7900
Wire Wire Line
	7150 5050 6250 5050
Wire Wire Line
	7550 4650 7550 7500
Wire Wire Line
	7450 4750 7450 7600
Wire Wire Line
	7350 4850 7350 7700
Wire Wire Line
	7250 4950 7250 7800
Wire Wire Line
	7150 5050 7150 7900
Connection ~ 7800 3650
Connection ~ 7800 1450
Wire Wire Line
	7800 1450 7800 3650
Connection ~ 8100 4500
Wire Wire Line
	8100 4500 9000 4500
Connection ~ 8200 4400
Wire Wire Line
	8200 4400 7200 4400
$Sheet
S 1050 900  5200 6350
U 5C7CF551
F0 "Badge_uC" 50
F1 "Badge_uC.sch" 50
F2 "PMIC_PWR_HLD" I R 6250 2100 50 
F3 "SDA" I R 6250 3700 50 
F4 "SCL" I R 6250 3800 50 
F5 "1V8" I R 6250 1450 50 
F6 "3V3" I R 6250 1550 50 
F7 "5V0" I R 6250 1650 50 
F8 "PMIC_AMUX" I R 6250 2200 50 
F9 "PMIC_nIRQ" I R 6250 2300 50 
F10 "ECG_CSB" I R 6250 4650 50 
F11 "ECG_SDI" I R 6250 4750 50 
F12 "ECG_SDO" I R 6250 4850 50 
F13 "ECG_SCLK" I R 6250 4950 50 
F14 "ECG_INT" I R 6250 5050 50 
F15 "Sensor_BMA400_INT1" I R 6250 3500 50 
F16 "Sensor_BHI_INT" I R 6250 3400 50 
F17 "PMIC_nRST" I R 6250 2600 50 
F18 "32kHz" I R 6250 6200 50 
F19 "UV_LED0" I R 6250 1100 50 
F20 "UV_LED1" I R 6250 1200 50 
F21 "UV_LED2" I R 6250 1300 50 
F22 "PMIC_nEN" I R 6250 2750 50 
F23 "CHG_IN" I R 6250 1850 50 
F24 "ECG_P" I R 6250 6550 50 
F25 "ECG_N" I R 6250 6650 50 
F26 "ECG_VCM" I R 6250 6750 50 
F27 "BAT+" I R 6250 1750 50 
F28 "PMIC_GPIO" I R 6250 1950 50 
F29 "ECG_AOUT" I R 6250 6850 50 
$EndSheet
Wire Wire Line
	9000 8200 6950 8200
Wire Wire Line
	6950 8200 6950 6200
Wire Wire Line
	6950 6200 6250 6200
Wire Wire Line
	6250 1550 9000 1550
Wire Wire Line
	6250 1100 9000 1100
Wire Wire Line
	9000 1200 6250 1200
Wire Wire Line
	6250 1300 9000 1300
Wire Wire Line
	6250 2750 9000 2750
Wire Wire Line
	6250 1850 9000 1850
Wire Wire Line
	6250 1750 9000 1750
Wire Wire Line
	9000 6550 6250 6550
Wire Wire Line
	6250 6650 9000 6650
Wire Wire Line
	9000 6750 6250 6750
Wire Wire Line
	6250 1950 9000 1950
Wire Wire Line
	6250 1650 9000 1650
Wire Wire Line
	9000 2500 8100 2500
Wire Wire Line
	8100 2500 8100 4500
Wire Wire Line
	9000 2400 8200 2400
Wire Wire Line
	8200 2400 8200 4400
Wire Wire Line
	9000 8300 6850 8300
Wire Wire Line
	6850 8300 6850 6850
Wire Wire Line
	6850 6850 6250 6850
Wire Wire Line
	6250 2600 9000 2600
Wire Wire Line
	7800 3650 7800 7000
$Comp
L Graphic:Logo_Open_Hardware_Large LOGO101
U 1 1 5D6F126A
P 1550 8400
F 0 "LOGO101" H 1550 8900 50  0001 C CNN
F 1 "Logo_Open_Hardware_Large" H 1550 8000 50  0001 C CNN
F 2 "Symbol:OSHW-Logo2_7.3x6mm_SilkScreen" H 1550 8400 50  0001 C CNN
F 3 "~" H 1550 8400 50  0001 C CNN
	1    1550 8400
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID101
U 1 1 5F3EEBF0
P 2700 8450
F 0 "FID101" H 2785 8496 50  0000 L CNN
F 1 "Fiducial" H 2785 8405 50  0000 L CNN
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 2700 8450 50  0001 C CNN
F 3 "~" H 2700 8450 50  0001 C CNN
	1    2700 8450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID103
U 1 1 5F3EF61C
P 3200 8450
F 0 "FID103" H 3285 8496 50  0000 L CNN
F 1 "Fiducial" H 3285 8405 50  0000 L CNN
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 3200 8450 50  0001 C CNN
F 3 "~" H 3200 8450 50  0001 C CNN
	1    3200 8450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID105
U 1 1 5F3EF903
P 3700 8450
F 0 "FID105" H 3785 8496 50  0000 L CNN
F 1 "Fiducial" H 3785 8405 50  0000 L CNN
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 3700 8450 50  0001 C CNN
F 3 "~" H 3700 8450 50  0001 C CNN
	1    3700 8450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID106
U 1 1 5F3EF9FD
P 4150 8450
F 0 "FID106" H 4235 8496 50  0000 L CNN
F 1 "Fiducial" H 4235 8405 50  0000 L CNN
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 4150 8450 50  0001 C CNN
F 3 "~" H 4150 8450 50  0001 C CNN
	1    4150 8450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID102
U 1 1 5F3EFB30
P 2700 8750
F 0 "FID102" H 2785 8796 50  0000 L CNN
F 1 "Fiducial" H 2785 8705 50  0000 L CNN
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 2700 8750 50  0001 C CNN
F 3 "~" H 2700 8750 50  0001 C CNN
	1    2700 8750
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID104
U 1 1 5F3EFD75
P 3200 8750
F 0 "FID104" H 3285 8796 50  0000 L CNN
F 1 "Fiducial" H 3285 8705 50  0000 L CNN
F 2 "Fiducial:Fiducial_0.75mm_Mask1.5mm" H 3200 8750 50  0001 C CNN
F 3 "~" H 3200 8750 50  0001 C CNN
	1    3200 8750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
